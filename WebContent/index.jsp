<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Professional-Ajax</title>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css" />

<style type="text/css">
.pretty-hover,#hover-demo1 p:hover {
    background: #ff0;
}
</style>

<script type="text/javascript">
    $("li").hover(function() {
        $(this).append($("<span> ***</span>"));
    }, function() {
        $(this).find("span:last").remove();
    });
    
    $('#hover-demo2 a').hover(function() {
        $(this).addClass('pretty-hover');
      }, function() {
        $(this).removeClass('pretty-hover');
      });
</script>
</head>
<body>
    <div class="result">minhhandsome</div>
    <ol>
        <li class="hover-demo2"><a href="" class="result">Chapter 01: What is Ajax?</a></li>
        <li><a href="">Chapter 02: Ajax Basics</a>
            <ol>
                <li>hiddenFrame
                    <ol>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\DataDisplay.jsp">DataDisplay.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\DataEntry.jsp">DataEntry.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\GetCustomerData.jsp">GetCustomerData.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\HiddenFrameGet.jsp">HiddenFrameGet.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\HiddenFramePost.jsp">HiddenFramePost.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.1.hiddenFrame\SaveCustomer.jsp">DataDisplay.jsp</a></li>
                    </ol>
                </li>
                <li>hiddenIFrame
                    <ol>
                        <li>HiddenDynamicIFrameGET.jsp</li>
                        <li>HiddenDynamicIFramePOST.jsp</li>
                        <li>HiddenInlineIFrameGET.jsp</li>
                    </ol>
                </li>
                <li>XHRs
                    <ol>
                        <li>XHRGet.jsp</li>
                        <li>XHRPost.jsp</li>
                    </ol>
                </li>
                <li>image
                    <ol>
                        <li><a href="Chapter02-AjaxBasics\2.2.3.images\ImageGetCustomerData.jsp">Image1.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\2.2.3.images\ImageUpdateCustomer.jsp">Image2.jsp</a></li>
                    </ol>
                </li>
                <li>DynamicScriptLoadings
                    <ol>
                        <li><a href="Chapter02-AjaxBasics\DynamicScriptLoadings\DynamicScriptLoading1.jsp">DynamicScriptLoading1.jsp</a></li>
                        <li><a href="Chapter02-AjaxBasics\DynamicScriptLoadings\DynamicScriptLoading2.jsp">DynamicScriptLoading2.jsp</a></li>
                    </ol>
                </li>
            </ol>
        </li>
        <li><a href="">Chapter 03: Ajax Patterns</a>
            <ol>
                <li>CommunicationControlPatterns
                    <ol>
                        <li>Multi-stageDownload
                            <ol>
                                <a href="Chapter03-AjaxPatterns\CommunicationControlPatterns\Multi-stageDownload\Article.jsp">Article.jsp</a>
                            </ol>
                        </li>
                        <li>PeriodicRefresh
                            <ol>
                                <a href="Chapter03-AjaxPatterns\CommunicationControlPatterns\PeriodicRefresh\Comment.jsp">Comment.jsp</a>
                            </ol>
                            <ol>
                                <a href="Chapter03-AjaxPatterns\CommunicationControlPatterns\PeriodicRefresh\Example.jsp">Example.jsp</a>
                            </ol>
                        </li>
                        <li>PredictiveFetch
                            <ol>
                                <a href="Chapter03-AjaxPatterns\CommunicationControlPatterns\PredictiveFetch\Article.jsp">Article.jsp</a>
                            </ol>
                        </li>
                        <li>SubmissionThrottling
                            <ol>IncrementalFieldValidation.jsp
                            </ol>
                            <ol>IncrementalFormValidation.jsp
                            </ol>
                        </li>
                    </ol>
                </li>
                <li>FallbackPatterns
                    <ol>
                        <li>CancelPendingRequests
                            <ol>
                                <li><a href="Chapter03-AjaxPatterns\FallbackPatterns\CancelPendingRequests\Comment.jsp">Comment.jsp</a></li>
                            </ol>
                        </li>
                        <li>TryAgainLater
                            <ol>
                                <li><a href="Chapter03-AjaxPatterns\FallbackPatterns\TryAgainLater\Article.htm">Article.htm</a></li>
                            </ol>
                        </li>
                    </ol>
                </li>
            </ol></li>
        <li><a href="">Chapter 04: AjaxLibraries</a>
            <ol>
                <li>ConnectionManagers</li>
                <li>jQuerys
                    <ol>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryAjaxGet.jsp">jQueryAjaxGet.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryAjaxPost.jsp">jQueryAjaxPost.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryAjaxStart.jsp">jQueryAjaxStart.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryGet.jsp">jQueryGet.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryLoadGet.jsp">jQueryLoadGet.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryLoadPost.jsp">jQueryLoadPost.jsp</a></li>
                        <li><a href="Chapter04-AjaxLibraries\jQuerys\jQueryPost.jsp">jQueryPost.jsp</a></li>
                    </ol>
                </li>
                <li>Prototypes</li>
            </ol></li>
        <li><a href="">Chapter 05: Request Management</a>
            <ol>
                <li><a href="Chapter05-Request Management\PriorityQueue.htm">PriorityQueue.htm</a></li>
                <li><a href="Chapter05-Request Management\RequestManager.htm">RequestManager.htm</a></li>
            </ol></li>
        <li><a href="">Chapter07: RSSandAtom</a>
            <ol>
                <li><a href="Chapter07-RSSandAtom\NewsTicker\index.htm">NewsTicker-index</a></li>
                <li><a href="Chapter07-RSSandAtom\WebSearch\index.htm">WebSearch-index</a></li>
            </ol></li>
        <li><a href="">Chapter08: json</a>
            <ol>
                <li>GeneralJSON
                    <ol>
                        <li><a href="Chapter08-json\GeneralJSON\JSONDecodingExample.jsp">JSONDecoding</a></li>
                        <li><a href="Chapter08-json\GeneralJSON\JSONEncodingExample.jsp">JSONEncoding</a></li>
                        <li><a href="Chapter08-json\GeneralJSON\JSONExample.jsp">JSON</a></li>
                        <li><a href="Chapter08-json\GeneralJSON\LiteralsExample.jsp">Literals</a></li>
                        <li><a href="Chapter08-json\GeneralJSON\MixingLiteralsExample.jsp">MixingLiterals</a></li>
                        <li><a href="Chapter08-json\GeneralJSON\MixingLiteralsExample2.jsp">MixingLiterals2</a></li>
                    </ol>
                </li>
                <li>Autosuggest
                    <ol>
                        <li><a href="Chapter08-json\Autosuggest\AutoSuggestExample.jsp">AutoSuggest</a></li>
                    </ol>
                </li>
            </ol></li>
        <li><a href="chartjs/line.jsp">Chart</a></li>
    </ol>
</body>
</html>